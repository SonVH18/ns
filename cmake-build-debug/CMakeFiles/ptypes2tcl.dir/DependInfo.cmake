# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/blej/ns-allinone-2.35/ns-2.35/common/ptypes2tcl.cc" "/home/blej/ns-allinone-2.35/ns-2.35/cmake-build-debug/CMakeFiles/ptypes2tcl.dir/common/ptypes2tcl.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CPP_NAMESPACE=std"
  "DEBUG"
  "HAVE_CONFIG_H"
  "HAVE_LIBOTCL1_14"
  "HAVE_LIBTCL8_5"
  "HAVE_LIBTCLCL"
  "HAVE_LIBTK8_5"
  "HAVE_OTCL_H"
  "HAVE_TCLCL_H"
  "HAVE_TCLINT_H"
  "HAVE_TCL_H"
  "HAVE_TK_H"
  "LINUX_TCP_HEADER"
  "NDEBUG"
  "NO_TK"
  "NS_DIFFUSION"
  "SMAC_NO_SYNC"
  "STL_NAMESPACE=std"
  "TCLCL_CLASSINSTVAR"
  "TCP_DELAY_BIND_ALL"
  "USE_SHM"
  "USE_SINGLE_ADDRESS_SPACE"
  "rng_test"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../common"
  "../"
  "/usr/include/tcl8.5"
  "/home/blej/ns-allinone-2.35/otcl-1.14"
  "/home/blej/ns-allinone-2.35/tclcl-1.20"
  "../tcp"
  "../sctp"
  "../link"
  "../queue"
  "../adc"
  "../apps"
  "../mac"
  "../mobile"
  "../trace"
  "../routing"
  "../tools"
  "../classifier"
  "../mcast"
  "../diffusion3/lib/main"
  "../diffusion3/lib"
  "../diffusion3/lib/nr"
  "../diffusion3/ns"
  "../diffusion3/filter_core"
  "../asim"
  "../qs"
  "../diffserv"
  "../satellite"
  "../wpan"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
