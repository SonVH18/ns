# udp data

set opt(tn) 80
set opt(interval_1) 50.0
set opt(interval) 3.0
set opt(cbr_start_1_) 100.0
set opt(cbr_start_) 200.0

set s(0)	1813	;	set d(0)	197
set s(1)	1575	;	set d(1)	197
set s(2)	1576	;	set d(2)	197
set s(3)	1596	;	set d(3)	197
set s(4)	1597	;	set d(4)	197
set s(5)	1602	;	set d(5)	197
set s(6)	1603	;	set d(6)	197
set s(7)	1616	;	set d(7)	197
set s(8)	1617	;	set d(8)	197
set s(9)	1618	;	set d(9)	197
set s(10)	1619	;	set d(10)	197
set s(11)	1620	;	set d(11)	197
set s(12)	1621	;	set d(12)	197
set s(13)	1622	;	set d(13)	197
set s(14)	1623	;	set d(14)	197
set s(15)	1624	;	set d(15)	197
set s(16)	1625	;	set d(16)	197
set s(17)	1627	;	set d(17)	197
set s(18)	1628	;	set d(18)	197
set s(19)	1631	;	set d(19)	197
set s(20)	1639	;	set d(20)	197
set s(21)	1640	;	set d(21)	197
set s(22)	1641	;	set d(22)	197
set s(23)	1642	;	set d(23)	197
set s(24)	1643	;	set d(24)	197
set s(25)	1644	;	set d(25)	197
set s(26)	1645	;	set d(26)	197
set s(27)	1646	;	set d(27)	197
set s(28)	1647	;	set d(28)	197
set s(29)	1648	;	set d(29)	197
set s(30)	1649	;	set d(30)	197
set s(31)	1650	;	set d(31)	197
set s(32)	1659	;	set d(32)	197
set s(33)	1660	;	set d(33)	197
set s(34)	1661	;	set d(34)	197
set s(35)	1662	;	set d(35)	197
set s(36)	1663	;	set d(36)	197
set s(37)	1664	;	set d(37)	197
set s(38)	1665	;	set d(38)	197
set s(39)	1666	;	set d(39)	197
set s(40)	1667	;	set d(40)	197
set s(41)	1668	;	set d(41)	197
set s(42)	1669	;	set d(42)	197
set s(43)	1670	;	set d(43)	197
set s(44)	1671	;	set d(44)	197
set s(45)	1672	;	set d(45)	197
set s(46)	1673	;	set d(46)	197
set s(47)	1674	;	set d(47)	197
set s(48)	1675	;	set d(48)	197
set s(49)	1676	;	set d(49)	197
set s(50)	1677	;	set d(50)	197
set s(51)	1678	;	set d(51)	197
set s(52)	1679	;	set d(52)	197
set s(53)	1680	;	set d(53)	197
set s(54)	1681	;	set d(54)	197
set s(55)	1682	;	set d(55)	197
set s(56)	1683	;	set d(56)	197
set s(57)	1684	;	set d(57)	197
set s(58)	1685	;	set d(58)	197
set s(59)	1686	;	set d(59)	197
set s(60)	1687	;	set d(60)	197
set s(61)	1688	;	set d(61)	197
set s(62)	1689	;	set d(62)	197
set s(63)	1690	;	set d(63)	197
set s(64)	1691	;	set d(64)	197
set s(65)	1692	;	set d(65)	197
set s(66)	1693	;	set d(66)	197
set s(67)	1694	;	set d(67)	197
set s(68)	1695	;	set d(68)	197
set s(69)	1696	;	set d(69)	197
set s(70)	1697	;	set d(70)	197
set s(71)	1698	;	set d(71)	197
set s(72)	1703	;	set d(72)	197
set s(73)	1704	;	set d(73)	197
set s(74)	1705	;	set d(74)	197
set s(75)	1706	;	set d(75)	197
set s(76)	1707	;	set d(76)	197
set s(77)	1708	;	set d(77)	197
set s(78)	1709	;	set d(78)	197
set s(79)	1710	;	set d(79)	197

for {set i 0} {$i < $opt(tn)} {incr i} {
    $mnode_($s($i)) setdest [$mnode_($d($i)) set X_] [$mnode_($d($i)) set Y_] 0

    set sink_($i) [new Agent/Null]
    set udp_($i) [new Agent/UDP]
    $ns_ attach-agent $mnode_($d($i)) $sink_($i)
    $ns_ attach-agent $mnode_($s($i)) $udp_($i)
    $ns_ connect $udp_($i) $sink_($i)
    $udp_($i) set fid_ 2

    #Setup a CBR over UDP connection
    set cbr_($i) [new Application/Traffic/CBR]
    $cbr_($i) attach-agent $udp_($i)
    $cbr_($i) set type_ CBR
    $cbr_($i) set packet_size_ 50
    $cbr_($i) set rate_ 0.1Mb
    $cbr_($i) set interval_ $opt(interval_1)
    #$cbr set random_ false

    $ns_ at [expr $opt(cbr_start_1_) + [expr $i - 1] * $opt(interval_1) / $opt(tn)] "$cbr_($i) start"
    $ns_ at [expr $opt(cbr_start_1_) + 99] "$cbr_($i) stop"

    # Setup a CBR over UDP connection
    set cbr2_($i) [new Application/Traffic/CBR]
    $cbr2_($i) attach-agent $udp_($i)
    $cbr2_($i) set type_ CBR
    $cbr2_($i) set packet_size_ 50
    $cbr2_($i) set rate_ 0.1Mb
    $cbr2_($i) set interval_ $opt(interval)

    $ns_ at [expr $opt(cbr_start_) + [expr $i - 1] * $opt(interval) / $opt(tn)] "$cbr2_($i) start"
    $ns_ at [expr $opt(stop) - 5] "$cbr2_($i) stop"
}
