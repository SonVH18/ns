# udp data

set opt(tn) 10
set opt(interval_1) 50.0
set opt(interval) 3.0
set opt(cbr_start_1_) 100.0
set opt(cbr_start_) 200.0

set s(0)	1878	;	set d(0)	153
set s(1)	1764	;	set d(1)	153
set s(2)	1766	;	set d(2)	153
set s(3)	1767	;	set d(3)	153
set s(4)	1797	;	set d(4)	153
set s(5)	1798	;	set d(5)	153
set s(6)	1799	;	set d(6)	153
set s(7)	1800	;	set d(7)	153
set s(8)	1801	;	set d(8)	153
set s(9)	1802	;	set d(9)	153

for {set i 0} {$i < $opt(tn)} {incr i} {
    $mnode_($s($i)) setdest [$mnode_($d($i)) set X_] [$mnode_($d($i)) set Y_] 0

    set sink_($i) [new Agent/Null]
    set udp_($i) [new Agent/UDP]
    $ns_ attach-agent $mnode_($d($i)) $sink_($i)
    $ns_ attach-agent $mnode_($s($i)) $udp_($i)
    $ns_ connect $udp_($i) $sink_($i)
    $udp_($i) set fid_ 2

    #Setup a CBR over UDP connection
    set cbr_($i) [new Application/Traffic/CBR]
    $cbr_($i) attach-agent $udp_($i)
    $cbr_($i) set type_ CBR
    $cbr_($i) set packet_size_ 50
    $cbr_($i) set rate_ 0.1Mb
    $cbr_($i) set interval_ $opt(interval_1)
    #$cbr set random_ false

    $ns_ at [expr $opt(cbr_start_1_) + [expr $i - 1] * $opt(interval_1) / $opt(tn)] "$cbr_($i) start"
    $ns_ at [expr $opt(cbr_start_1_) + 99] "$cbr_($i) stop"

    # Setup a CBR over UDP connection
    set cbr2_($i) [new Application/Traffic/CBR]
    $cbr2_($i) attach-agent $udp_($i)
    $cbr2_($i) set type_ CBR
    $cbr2_($i) set packet_size_ 50
    $cbr2_($i) set rate_ 0.1Mb
    $cbr2_($i) set interval_ $opt(interval)

    $ns_ at [expr $opt(cbr_start_) + [expr $i - 1] * $opt(interval) / $opt(tn)] "$cbr2_($i) start"
    $ns_ at [expr $opt(stop) - 5] "$cbr2_($i) stop"
}
