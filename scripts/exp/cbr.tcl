# udp data

set opt(tn) 4
set opt(interval_1) 50.0
set opt(interval) 10.0
set opt(cbr_start_1_) 100.0
set opt(cbr_start_) 200.0

set s(0)	1488	;	set d(0)	197
set s(1)	1492	;	set d(1)	197
set s(2)	1495	;	set d(2)	197
set s(3)	1497	;	set d(3)	197
set s(4)	1455	;	set d(4)	197
set s(5)	1458	;	set d(5)	197
set s(6)	1460	;	set d(6)	197
set s(7)	1506	;	set d(7)	197
set s(8)	1464	;	set d(8)	197
set s(9)	1422	;	set d(9)	197
set s(10)	1424	;	set d(10)	197
set s(11)	1615	;	set d(11)	197
set s(12)	1514	;	set d(12)	197
set s(13)	1428	;	set d(13)	197
set s(14)	1516	;	set d(14)	197
set s(15)	1296	;	set d(15)	197
set s(16)	1253	;	set d(16)	197
set s(17)	1166	;	set d(17)	197
set s(18)	1077	;	set d(18)	197
set s(19)	990	;	set d(19)	197
set s(20)	1400	;	set d(20)	197
set s(21)	1398	;	set d(21)	197
set s(22)	1352	;	set d(22)	197
set s(23)	1307	;	set d(23)	197
set s(24)	1305	;	set d(24)	197
set s(25)	1310	;	set d(25)	197
set s(26)	1312	;	set d(26)	197
set s(27)	1446	;	set d(27)	197
set s(28)	1316	;	set d(28)	197
set s(29)	1318	;	set d(29)	197
set s(30)	1408	;	set d(30)	197
set s(31)	1321	;	set d(31)	197
set s(32)	1367	;	set d(32)	197
set s(33)	1410	;	set d(33)	197
set s(34)	1368	;	set d(34)	197
set s(35)	1371	;	set d(35)	197
set s(36)	1372	;	set d(36)	197
set s(37)	1330	;	set d(37)	197
set s(38)	1332	;	set d(38)	197
set s(39)	1246	;	set d(39)	197
set s(40)	1379	;	set d(40)	197
set s(41)	1248	;	set d(41)	197
set s(42)	1250	;	set d(42)	197
set s(43)	1206	;	set d(43)	197
set s(44)	1120	;	set d(44)	197
set s(45)	1221	;	set d(45)	197
set s(46)	1131	;	set d(46)	197
set s(47)	1085	;	set d(47)	197
set s(48)	1135	;	set d(48)	197
set s(49)	1182	;	set d(49)	197
set s(50)	1093	;	set d(50)	197
set s(51)	1185	;	set d(51)	197
set s(52)	1187	;	set d(52)	197
set s(53)	1145	;	set d(53)	197
set s(54)	1058	;	set d(54)	197
set s(55)	1236	;	set d(55)	197
set s(56)	1238	;	set d(56)	197
set s(57)	1153	;	set d(57)	197
set s(58)	1194	;	set d(58)	197
set s(59)	1199	;	set d(59)	197
set s(60)	1157	;	set d(60)	197
set s(61)	1114	;	set d(61)	197
set s(62)	1117	;	set d(62)	197
set s(63)	1205	;	set d(63)	197
set s(64)	1043	;	set d(64)	197
set s(65)	1668	;	set d(65)	197
set s(66)	1002	;	set d(66)	197
set s(67)	954	;	set d(67)	197
set s(68)	866	;	set d(68)	197
set s(69)	1128	;	set d(69)	197

for {set i 0} {$i < $opt(tn)} {incr i} {
    $mnode_($s($i)) setdest [$mnode_($d($i)) set X_] [$mnode_($d($i)) set Y_] 0

    set sink_($i) [new Agent/Null]
    set udp_($i) [new Agent/UDP]
    $ns_ attach-agent $mnode_($d($i)) $sink_($i)
    $ns_ attach-agent $mnode_($s($i)) $udp_($i)
    $ns_ connect $udp_($i) $sink_($i)
    $udp_($i) set fid_ 2

    #Setup a CBR over UDP connection
    set cbr_($i) [new Application/Traffic/CBR]
    $cbr_($i) attach-agent $udp_($i)
    $cbr_($i) set type_ CBR
    $cbr_($i) set packet_size_ 50
    $cbr_($i) set rate_ 0.1Mb
    $cbr_($i) set interval_ $opt(interval_1)
    #$cbr set random_ false

    $ns_ at [expr $opt(cbr_start_1_) + [expr $i - 1] * $opt(interval_1) / $opt(tn)] "$cbr_($i) start"
    $ns_ at [expr $opt(cbr_start_1_) + 99] "$cbr_($i) stop"

    # Setup a CBR over UDP connection
    set cbr2_($i) [new Application/Traffic/CBR]
    $cbr2_($i) attach-agent $udp_($i)
    $cbr2_($i) set type_ CBR
    $cbr2_($i) set packet_size_ 50
    $cbr2_($i) set rate_ 0.1Mb
    $cbr2_($i) set interval_ $opt(interval)

    $ns_ at [expr $opt(cbr_start_) + [expr $i - 1] * $opt(interval) / $opt(tn)] "$cbr2_($i) start"
    $ns_ at [expr $opt(stop) - 5] "$cbr2_($i) stop"
}
