#ifndef SPEED_NEIGHBOR_H_
#define SPEED_NEIGHBOR_H_

#include <cstdio>
#include "speed_packet.h"
#include "../../config.h"

#define DEFAULT_ALPHA 0.7 // delay update alpha, 0.0 -> 1.0
#define DEFAULT_PROB_K 1  // probability update K, int >= 1
#define DEFAULT_RRPG_K 1.0 // relay ratio proportional gain K, 0.0 -> ?
#define DEFAULT_SPEED_SETPOINT 1000.0 //the speed set point m/s, > 0.0


struct speed_sink {
    double x_;
    double y_;

    struct speed_sink *next_;
};


struct F2list {

    struct node * node_list_;
    struct F2list *next_;

    ~F2list()
    {
        node* temp = node_list_;
        do {
            delete temp;
            temp = temp->next_;
        } while(temp && temp != node_list_);
    }
};

struct speed_neighbor {
    nsaddr_t id_;
    double x_;
    double y_;
    int seqno_; //the sequence number of the hello msg
    double send2delay_; //one hop delay estimation
    int fwd_counter_; //how many packets have been fwded by it
    int miss_counter_;
    double miss_ratio_;

    struct speed_neighbor *next_;

    struct speed_sink *sinks_;
    //the temporary variables used once after update
    double speed_;
    double prob_;
    double dis2sink_;
};


class SpeedNeighbors {
private:
    struct speed_neighbor *head_;
    struct speed_neighbor *tail_;
    int nbSize_;

    nsaddr_t myid_;
    double my_x_;
    double my_y_;

    double alpha_; //delay updating parameter alpha
    int prob_k_; //K used to calculate the probability, SNGF
    double rrpg_k_; //K used to miss ratio portional gain, Relay Ratio controller

    double speed_sp_; //speed set point

    bool fsmember(double, double);

    bool fwdCand(double);

    bool anyroute(struct speed_neighbor *, double, double);

    struct F2list * sngf(double, double, double);//calculate the SNGF routing
    struct F2list * maxSpeed(double, double); //SPEED-S routing
    struct F2list * minDelay(double, double); //SPEED-T routing
    struct F2list * shortest(double, double); //for congesting flow

    //the functions to provide the temporary variables in neighbor list
    void caldis(double, double); //calculate the distances of all neighbors
    void calSpeed(double, double); //function caldis() needs to be called first
    bool calProb(int, double, double); //function calSpeed() needs to be called first
    double relayRatio(double, double); //caldis() needs to be called first

    struct speed_neighbor *knownNB(nsaddr_t);

    void nort(nsaddr_t, double, double);

    void addnort(struct speed_neighbor *, double, double);

    RNG fwdProb_;

public:
    SpeedNeighbors();

    //alpha, prob K, rr controller k
    void setpara(double, int, double);

    //my id, location x, y
    void myinfo(nsaddr_t, double, double);

    //new neighbor: id, location x, y, miss ratio, delay
    void newNB(nsaddr_t, double, double, double, int);

    //update the send to delay of the neighbor id, dl, miss ratio, dst loc
    void updateDelay(nsaddr_t, double, double, double);

    void collide(nsaddr_t);

    //get the next hop based on different routing policies with sink x y
    struct F2list* nexthop(int, double, double, double);

    //get the average send to delay of all my FS members with sink x, y
    double avg2delay(double, double); //only called when congesting backpressure

    void dump(FILE *);

    double getdis(nsaddr_t, double, double);

    // E2E
    void setSpeedSetPoint(double);
};


#endif