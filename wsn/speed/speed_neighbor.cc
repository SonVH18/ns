#include <wsn/geomathhelper/geo_math_helper.h>
#include "speed_neighbor.h"

SpeedNeighbors::SpeedNeighbors() {
    head_ = tail_ = NULL;
    nbSize_ = 0;
    myid_ = -1;
    my_x_ = 0.0;
    my_y_ = 0.0;

    alpha_ = DEFAULT_ALPHA;
    prob_k_ = DEFAULT_PROB_K;
    rrpg_k_ = DEFAULT_RRPG_K;
    speed_sp_ = DEFAULT_SPEED_SETPOINT;

    for (int i = 0; i < 5; i++) {
        fwdProb_.reset_next_substream();
    }
}

struct speed_neighbor *
SpeedNeighbors::knownNB(nsaddr_t nid) {
    struct speed_neighbor *temp = head_;
    while (temp) {
        if (temp->id_ == nid)
            return temp;
        temp = temp->next_;
    }

    return NULL;
}


void
SpeedNeighbors::newNB(nsaddr_t nid, double nx, double ny,
                      double dl, int sq) {
    struct speed_neighbor *temp = knownNB(nid);
    if (temp != NULL) {
        updateDelay(nid, dl, nx, ny);
        temp->seqno_ = sq;
        return;
    }

    assert(dl > 0.0);

    temp = (struct speed_neighbor *) malloc(sizeof(struct speed_neighbor));

    temp->id_ = nid;
    temp->x_ = nx;
    temp->y_ = ny;
    temp->seqno_ = sq;
    temp->send2delay_ = dl;
    temp->fwd_counter_ = 1;
    temp->miss_counter_ = 0;

    temp->next_ = NULL;
    temp->sinks_ = NULL;

    if (tail_ == NULL) {
        head_ = tail_ = temp;
        nbSize_ = 1;
    } else {
        tail_->next_ = temp;
        tail_ = temp;
        nbSize_++;
    }
}

void
SpeedNeighbors::setpara(double alpha, int prob_k, double pg_k) {
    assert(alpha <= 1.0);
    alpha_ = alpha;
    assert(prob_k >= 1);
    prob_k_ = prob_k;

    rrpg_k_ = pg_k;
}

void
SpeedNeighbors::myinfo(nsaddr_t myid, double myx, double myy) {
    myid_ = myid;
    my_x_ = myx;
    my_y_ = myy;
}

void
SpeedNeighbors::updateDelay(nsaddr_t nid, double dl,
                            double sx, double sy) {
    struct speed_neighbor *temp = knownNB(nid);

    if (temp == NULL)
        return;

    assert(dl > 0.0);

    double mydis = G::distance(my_x_, my_y_, sx, sy);
    double nbdis = G::distance(temp->x_, temp->y_, sx, sy);

    assert(mydis > nbdis);

    temp->send2delay_ =
            alpha_ * dl + (1.0 - alpha_) * temp->send2delay_;

    temp->fwd_counter_++;

    double tempspeed = (mydis - nbdis) / dl;

    if (tempspeed < speed_sp_) temp->miss_counter_++;

    temp->miss_ratio_ = alpha_* temp->miss_counter_/temp->fwd_counter_ + (1.0 - alpha_)* temp->miss_ratio_;

    if (dl > 0.8 * INFINITE_DELAY)
        nort(nid, sx, sy);
}

F2list*
SpeedNeighbors::nexthop(int type, double sx, double sy, double current_e2e_reliability_) {
    switch (type) {
        case SPEED_SNGF:
            return sngf(sx, sy, current_e2e_reliability_);

        case SPEED_S:
            return maxSpeed(sx, sy);

        case SPEED_T:
            return minDelay(sx, sy);

        case SPEED_CONG: //this is just used by the congesting flow
            return shortest(sx, sy);

        default:
            printf("Wrong speed type, check the code.\n");
            exit(1);
    }
}

bool
SpeedNeighbors::anyroute(struct speed_neighbor *nb, double dx, double dy) {
    struct speed_sink *temp = nb->sinks_;
    double diffX;
    double diffY;
    while (temp) {
        diffX = temp->x_ - dx;
        diffY = temp->y_ - dy;
        if (-0.1 <= diffX && diffX <= 0.1 &&
            -0.1 <= diffY && diffY <= 0.1)
            return false;
        temp = temp->next_;
    }
    return true;
}

//to check the fwd set membership based on distance to the sink
bool
SpeedNeighbors::fsmember(double mydis, double nbdis) {
    return mydis > nbdis;
}

bool
SpeedNeighbors::fwdCand(double speed) {
    return speed >= speed_sp_;
}


void
SpeedNeighbors::caldis(double sx, double sy) {
    struct speed_neighbor *temp = head_;
    while (temp) {
        temp->dis2sink_ = G::distance(temp->x_, temp->y_, sx, sy);
        temp = temp->next_;
    }
}

void
SpeedNeighbors::calSpeed(double sx, double sy) {
    struct speed_neighbor *temp = head_;
    double mydis = G::distance(my_x_, my_y_, sx, sy);
    while (temp) {
        assert(delay_ > 0.0);
        temp->speed_ = (mydis - temp->dis2sink_) / temp->send2delay_;
        temp = temp->next_;
    }
}

//to calculate all the probabilities of fwd Candidate
//type 0: (Relay Ratio controller is off) check the prob of fwd candidates,
//type 1: (Relay Ratio controller is on) check it of fs member
bool
SpeedNeighbors::calProb(int type, double sx, double sy) {
    struct speed_neighbor *temp;

    double mydis = G::distance(my_x_, my_y_, sx, sy);
    double amountspeed = 0.0;
    double tempspeedk;

    if (type == 0) {
        temp = head_;

        while (temp) {
            if (//!--anyroute(temp, sx, sy) &&
                    fsmember(mydis, temp->dis2sink_) &&
                    fwdCand(temp->speed_)) {
                tempspeedk = 1.0;
                for (int i = 0; i < prob_k_; i++) {
                    tempspeedk *= temp->speed_;
                }
                amountspeed += tempspeedk;
                temp->prob_ = amountspeed;//temp->speed is temporary
            }
            temp = temp->next_;
        } //while

        if (amountspeed == 0.0) return false;

        temp = head_;

        while (temp) {
            if (//!--anyroute(temp, sx, sy) &&
                    fsmember(mydis, temp->dis2sink_) &&
                    fwdCand(temp->speed_)) {
                temp->prob_ = temp->prob_ / amountspeed;
            }
            temp = temp->next_;
        }//while
        return true;
    } else if (type == 1) {
        temp = head_;

        while (temp) {
            if (//!--anyroute(temp, sx, sy) &&
                    fsmember(mydis, temp->dis2sink_)) {
                tempspeedk = 1.0;
                for (int i = 0; i < prob_k_; i++) {
                    tempspeedk *= temp->speed_;
                }
                amountspeed += tempspeedk;
                temp->prob_ = amountspeed;//temp->speed is temporary
            }
            temp = temp->next_;
        } //while

        if (amountspeed == 0.0) return false;

        temp = head_;

        while (temp) {
            if (//!--anyroute(temp, sx, sy) &&
                    fsmember(mydis, temp->dis2sink_)) {
                temp->prob_ = temp->prob_ / amountspeed;
            }
            temp = temp->next_;
        }//while
        return true;

    } else {
        printf("Wrong prob mode, check code.\n");
        exit(1);
    }
}


//SNGF routing
//return -1: No route, back pressure Infinite delay
//return -2: Congestion back pressure avg2delay
//return >=0: the forwarding neighbor with this id
F2list*
SpeedNeighbors::sngf(double sx, double sy, double current_e2e_reliability_) {
    caldis(sx, sy);
    calSpeed(sx, sy);

    double reliability = 0;
    F2list * F2 = new  F2list();
    F2->next_ = NULL;
    F2->node_list_ = NULL;

    if (nbSize_ <= 0) {
        F2->node_list_->id_ = -1;
        return F2;
    }

    double mydis = G::distance(my_x_, my_y_, sx, sy);

    //to call the calProb() function
    //type 0: The relay ratio controller is off
    //type 1: if type 0 fails, turn on the relay ratio and try again.
    if (calProb(0, sx, sy)) {
        double prob = fwdProb_.uniform(0.0, 1.0);
        struct speed_neighbor *temp = head_;
        while (temp) {
            if (//!--anyroute(temp, sx, sy) &&
                    fsmember(mydis, temp->dis2sink_) &&
                    fwdCand(temp->speed_)) {
                if (prob <= temp->prob_) {
                    double RQ = pow((1 - temp->miss_ratio_), round(G::distance(temp->x_,temp->y_,sx,sy)/G::distance(my_x_,my_y_,temp->x_, temp->y_)) + 1);
                    reliability = 1 - (1 - reliability)*(1 - RQ);
                    node * f1 = new node();
                    f1->id_ = temp->id_;
                    f1->x_ = RQ * (current_e2e_reliability_) / reliability;
                    f1->next_ = F2->node_list_;
                    F2->node_list_ = f1;
                }
            }
            temp = temp->next_;
        }
        if (F2 != NULL) return F2;

        //Error happens if reach here
        printf("Error: sngf 1.\n");
        exit(1);
    }

    //  calSpeed(sx, sy);

    if (calProb(1, sx, sy)) {
        double rlyratio = relayRatio(sx, sy);
        double fwdprob = fwdProb_.uniform(0.0, 1.0);

        if (fwdprob > rlyratio) {
            F2->node_list_->id_ = -2;
            return  F2;
        }

        fwdprob = fwdProb_.uniform(0.0, 1.0);
        struct speed_neighbor *temp = head_;
        while (temp) {
            if (//!--anyroute(temp, sx, sy) &&
                    fsmember(mydis, temp->dis2sink_)) {
                if (fwdprob <= temp->prob_) {
                    double RQ = pow((1 - temp->miss_ratio_), round(G::distance(temp->x_,temp->y_,sx,sy)/G::distance(my_x_,my_y_,temp->x_, temp->y_)) + 1);
                    reliability = 1 - (1 - reliability)*(1 - RQ);
                    node * f1 = new node();
                    f1->id_ = temp->id_;
                    f1->x_ = RQ * (current_e2e_reliability_) / reliability;
                    f1->next_ = F2->node_list_;
                    F2->node_list_ = f1;
                };
            }
            temp = temp->next_;
        }
        if (F2 != NULL) return F2;

        //Error happens if reach here
        printf("Error sngf 2.\n");
        exit(1);
    }

    //return -1; //No route
}

double
SpeedNeighbors::relayRatio(double sx, double sy) {

    double mydis = G::distance(my_x_, my_y_, sx, sy);
    struct speed_neighbor *temp = head_;

    double amountratio = 0.0;
    int numfs = 0;

    while (temp) {
        if (//!--anyroute(temp, sx, sy) &&
                fsmember(mydis, temp->dis2sink_)) {
            //calculate the miss ratio first
            amountratio += ((double) temp->miss_counter_ / (double) temp->fwd_counter_);
            if (amountratio == 0.0) return 1.0;
            numfs++;
        }
        temp = temp->next_;
    }

    //the relay ratio = 1.0 - portional gain * average miss ratio
    amountratio = 1.0 - rrpg_k_ * (amountratio / (double) numfs);

    return amountratio;
}


double
SpeedNeighbors::avg2delay(double sx, double sy) {
    struct speed_neighbor *temp = head_;
    double mydis = G::distance(my_x_, my_y_, sx, sy);

    double amountdelay = 0.0;
    int numfs = 0;

    caldis(sx, sy);

    while (temp) {
        if (//!--anyroute(temp, sx, sy) &&
                fsmember(mydis, temp->dis2sink_)) {
            amountdelay += temp->send2delay_;
            numfs++;
        }
        temp = temp->next_;
    }

    if (numfs == 0) {
        amountdelay = INFINITE_DELAY;
    } else {
        amountdelay = amountdelay / (double) numfs;
    }

    return amountdelay;
}


F2list*
SpeedNeighbors::maxSpeed(double sx, double sy) {
    double mydis = G::distance(my_x_, my_y_, sx, sy);
    struct speed_neighbor *temp = head_;

    double max_speed = 0.0;
    nsaddr_t nexthop = -1;

    caldis(sx, sy);
    calSpeed(sx, sy);

    while (temp) {
        if (//!--anyroute(temp, sx, sy) &&
                fsmember(mydis, temp->dis2sink_) &&
                temp->speed_ > max_speed) {
            max_speed = temp->speed_;
            nexthop = temp->id_;
        }
        temp = temp->next_;
    }
    F2list * F2 = new  F2list();
    F2->next_ = NULL;
    F2->node_list_ = NULL;
    return F2; //if nexthop = -1, then no route
}


F2list *
SpeedNeighbors::minDelay(double sx, double sy) {
    double mydis = G::distance(my_x_, my_y_, sx, sy);
    struct speed_neighbor *temp = head_;

    caldis(sx, sy);

    double min_delay = INFINITE_DELAY;
    nsaddr_t nexthop = -1;

    while (temp) {
        if (//!--anyroute(temp, sx, sy) &&
                fsmember(mydis, temp->dis2sink_) &&
                min_delay > temp->send2delay_) {
            min_delay = temp->send2delay_;
            nexthop = temp->id_;
        }
        temp = temp->next_;
    }

    F2list * F2 = new  F2list();
    F2->next_ = NULL;
    F2->node_list_ = NULL;
    return F2; //if nexthop == -1, then no route
}


F2list *
SpeedNeighbors::shortest(double sx, double sy) {
    caldis(sx, sy);
    double shortest_dis = 100000000000.0;
    nsaddr_t nexthop = -1;
    struct speed_neighbor *temp = head_;
    while (temp) {
        if (temp->dis2sink_ < shortest_dis) {
            shortest_dis = temp->dis2sink_;
            nexthop = temp->id_;
        }
        temp = temp->next_;
    }
    F2list * F2 = new  F2list();
    F2->next_ = NULL;
    F2->node_list_ = NULL;
    return F2;
}

void
SpeedNeighbors::collide(nsaddr_t nid) {
    struct speed_neighbor *temp = head_;
    while (temp) {
        if (temp->id_ == nid) {
            temp->fwd_counter_++;
            temp->miss_counter_++;
            return;
        }
        temp = temp->next_;
    }
}


void
SpeedNeighbors::dump(FILE *fp) {
    for (struct speed_neighbor *tmp = head_; tmp; tmp = tmp->next_) {
        fprintf(fp, "%d,", tmp->id_);
    }
}


void
SpeedNeighbors::nort(nsaddr_t nid, double dx, double dy) {
    struct speed_neighbor *temp = head_;

    while (temp) {
        if (temp->id_ == nid) {
            addnort(temp, dx, dy);
            return;
        }
        temp = temp->next_;

    }

}

void
SpeedNeighbors::addnort(struct speed_neighbor *nb, double dx, double dy) {
    struct speed_sink *temp = nb->sinks_;
    while (temp) {
        if (temp->x_ == dx && temp->y_ == dy)return;
        temp = temp->next_;
    }

    temp = (struct speed_sink *) malloc(sizeof(struct speed_sink));

    temp->x_ = dx;
    temp->y_ = dy;

    temp->next_ = nb->sinks_;
    nb->sinks_ = temp;

}


double
SpeedNeighbors::getdis(nsaddr_t nid, double sx, double sy) {
    struct speed_neighbor *temp = knownNB(nid);

    if (temp == NULL) return 100000000000.0;

    double tempx = temp->x_ - sx;
    double tempy = temp->y_ - sy;

    tempx = tempx * tempx;
    tempy = tempy * tempy;

    return sqrt(tempx + tempy);
}

void SpeedNeighbors::setSpeedSetPoint(double stp) {
    speed_sp_ = stp;
}
