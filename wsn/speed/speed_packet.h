#ifndef SPEED_PACKET_H_
#define SPEED_PACKET_H_

#include "config.h"
#include "agent.h"
#include "ip.h"
#include "address.h"
#include "scheduler.h"
#include "mobilenode.h"
#include "tools/random.h"
#include <math.h>


#define  SPEED_SNGF 0
#define  SPEED_S    1
#define  SPEED_T    2
#define  SPEED_CONG 3


#define SPEED_CURRENT Scheduler::instance().clock()

#define INFINITE_DELAY   500000000000000.9

#define SPEEDTYPE_HELLO   0x01
#define SPEEDTYPE_DATA    0x02
#define SPEEDTYPE_BKP     0x03
#define SPEEDTYPE_ACK     0x04

#define HDR_SPEED(p) ((struct hdr_speed*)hdr_speed::access(p))
#define HDR_SPEED_HELLO(p) ((struct hdr_speed_hello*)hdr_speed::access(p))
#define HDR_SPEED_BKP(p) ((struct hdr_speed_bkp*)hdr_speed::access(p))
#define HDR_SPEED_DATA(p) ((struct hdr_speed_data*)hdr_speed::access(p))
#define HDR_SPEED_ACK(p) ((struct hdr_speed_ack*)hdr_speed::access(p))

struct hdr_speed {
    u_int8_t type_;

    static int offset_;
    inline static int& offset() {return offset_;}
    inline static hdr_speed* access(const Packet* p){
        return (hdr_speed*) p->access(offset_);
    }
};


struct hdr_speed_hello {
    u_int8_t type_;
    nsaddr_t id_;
    double x_;
    double y_;
    double insert_time_;
    int seqno_;

    inline int size(){
        int sz =
                sizeof(u_int8_t)
                + sizeof(nsaddr_t)
                + 3*sizeof(double)
                + sizeof(int);
        return sz;
    }
};


struct hdr_speed_data {
    u_int8_t type_;
    nsaddr_t src_; //the src id
    nsaddr_t dst_; //the sink id
    double dx_; //the geographical info of sink
    double dy_;
    double crt_time_; //the creating time
    double insert_time_;
    nsaddr_t last_hop_;
    int hops_;
    int seqno_;

    double setpoint_;
    double e2e_delay_;
    double e2e_reliabitily_;

    inline int size(){
        int sz =
                sizeof(u_int8_t)
                + 3*sizeof(nsaddr_t)
                + 4*sizeof(double)
                + 2*sizeof(int);
                + 1*sizeof(double);
        return sz;
    }

};


struct hdr_speed_ack {
    u_int8_t type_;
    nsaddr_t id_;
    double dx_;
    double dy_;
    double delay_;

    inline int size(){
        int sz =
                sizeof(u_int8_t)
                + sizeof(nsaddr_t)
                + 3*sizeof(double);
        return sz;
    }

};


struct hdr_speed_bkp {
    u_int8_t type_;
    nsaddr_t id_; //who back pressure the packet(mostly not the sink)
    double sx_; //the geographical info of sink
    double sy_;
    double delay_;

    inline int size(){
        int sz =
                sizeof(u_int8_t)
                + sizeof(nsaddr_t)
                + 3*sizeof(double);
        return sz;
    }

};

union hdr_all_speed {
    hdr_speed         sh;
    hdr_speed_hello   shh;
    hdr_speed_bkp     sbh;
    hdr_speed_data    sdh;
    hdr_speed_ack     sah;
};


#endif