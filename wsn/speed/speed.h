#ifndef SPEED_H
#define SPEED_H

#include <vector>
#include "config.h"
#include "mac/mac-802_11.h"
#include "agent.h"
#include "timer-handler.h"
#include "mobilenode.h"
#include "classifier-port.h"
#include "cmu-trace.h"

#include "speed_packet.h"
#include "node.h"

#include "wsn/geomathhelper/geo_math_helper.h"
#include "../../config.h"
#include "speed_neighbor.h"

class SpeedAgent;

class SpeedHelloTimer : public TimerHandler {
public:
    SpeedHelloTimer(SpeedAgent *a) : TimerHandler() { a_ = a; }

protected:
    virtual void expire(Event *e);

    SpeedAgent *a_;
};

class SpeedAgent : public Point, public Agent {
protected:
    MobileNode *node_;                // the attached mobile node
    PortClassifier *port_dmux_;            // for the higher layer app de-multiplexing
    Trace *trace_target_;

    int random_pub_; //whether using the random publishing policy

    nsaddr_t my_id_;                    // node id (address), which is NOT necessary

    SpeedNeighbors *nblist_;

    int hello_seqno_;
    int pub_seqno_;

    int fwd_counter_;
    int miss_counter_;
    int bkp_counter_;

    SpeedHelloTimer hello_timer_;

    //the agent variables bind to tcl script variables
    double hello_period_;
    double pub_period_;
    int speed_type_;   //0: SPEED-SNGF, 1: SPEED-S, 2: SPEED-T
    double e2e_delay_;
    double delay_alpha_;
    int prob_k_;
    double rrpg_k_;
    Point *dest;                // position of destination

    void hellomsg();

    void pubdata(Packet *);

    //backpressure type 0: no route, 1 congestion with sink x, y
    void backpressure(int, double, double);

    void recvHello(Packet *);

    void recvData(Packet *);

    void recvBkp(Packet *); //received the back pressure packet

    void sendAck(nsaddr_t, double, double, double);

    void recvAck(Packet *);

    void relayData(Packet *);

    void checkmiss(nsaddr_t, double, double, double);

    void dumpEnergy();

    void dumpNeighbor();

    void dumpDrop(const char*, struct hdr_speed_data *sdh);

public:
    SpeedAgent();

    int command(int, const char *const *);

    void recv(Packet *, Handler *);

    void hellotimeout();

    void newdelay(nsaddr_t, double, double, double);

    void collide(nsaddr_t);

private:
    RNG randHello_;

    void startUp();                        // Initialize the Agent
};

#endif
