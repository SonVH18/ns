#include "speed.h"

#include "../include/tcl.h"
#include "speed_neighbor.h"
#include "../../common/packet.h"
#include "../geomathhelper/geo_math_helper.h"

int hdr_speed::offset_;

static class SpeedHeaderClass : public PacketHeaderClass {
public:
    SpeedHeaderClass() : PacketHeaderClass("PacketHeader/SPEED",
                                           sizeof(hdr_all_speed)) {
        bind_offset(&hdr_speed::offset_);
    }
} class_speedhdr;

static class SpeedAgentClass : public TclClass {
public:
    SpeedAgentClass() : TclClass("Agent/SPEED") {}

    TclObject *create(int, const char *const *) {
        return (new SpeedAgent());
    }
} class_speed;


void
SpeedHelloTimer::expire(Event *e) {
    a_->hellotimeout();
}

SpeedAgent::SpeedAgent() : Agent(PT_SPEED),
                           hello_timer_(this),
                           my_id_(-1),
                           hello_seqno_(0), pub_seqno_(0),
                           fwd_counter_(1), bkp_counter_(0),
                           miss_counter_(0) {
    this->x_ = -1.0;
    this->y_ = -1.0;
    port_dmux_ = NULL;
    trace_target_ = NULL;
    node_ = NULL;
    dest = new Point();
    e2e_delay_ = 0;


    bind("hello_period_", &hello_period_);
    bind("speed_type_", &speed_type_);
    bind("delay_alpha_", &delay_alpha_);
    bind("prob_k_", &prob_k_);
    bind("rrpg_k_", &rrpg_k_);
    //  bind("randomness", &random_pub_);

    for (int i = 0; i < 3; i++)
        randHello_.reset_next_substream();

    nblist_ = new SpeedNeighbors();

}

void
SpeedAgent::hellotimeout() {
    hellomsg();
    if (hello_period_ > 0) hello_timer_.resched(hello_period_);
}

void
SpeedAgent::hellomsg() {
    Packet *p = allocpkt();
    struct hdr_cmn *cmh = HDR_CMN(p);
    struct hdr_ip *iph = HDR_IP(p);
    struct hdr_speed_hello *shh = HDR_SPEED_HELLO(p);

    cmh->next_hop_ = IP_BROADCAST;
    cmh->addr_type_ = NS_AF_INET;
    cmh->ptype() = PT_SPEED;
    cmh->size() = shh->size() + IP_HDR_LEN;

    iph->daddr() = IP_BROADCAST << Address::instance().nodeshift();
    iph->sport() = RT_PORT;
    iph->dport() = RT_PORT;
    iph->ttl_ = 1;

    shh->type_ = SPEEDTYPE_HELLO;
    shh->id_ = my_id_;
    shh->x_ = this->x_;
    shh->y_ = this->y_;
    shh->insert_time_ = SPEED_CURRENT;
    shh->seqno_ = hello_seqno_++;

    send(p, 0);
}

void
SpeedAgent::pubdata(Packet *p) {
    pub_seqno_++;

    struct hdr_cmn *cmh = HDR_CMN(p);
    struct hdr_ip *iph = HDR_IP(p);
    struct hdr_speed_data *sdh = HDR_SPEED_DATA(p);

    cmh->direction() = hdr_cmn::DOWN;
    cmh->addr_type_ = NS_AF_INET;
//    cmh->ptype() = PT_SPEED;
    cmh->size() = sdh->size() + IP_HDR_LEN;

    sdh->type_ = SPEEDTYPE_DATA;
    sdh->src_ = my_id_;
    sdh->dst_ = iph->daddr();;
    sdh->last_hop_ = my_id_;
    sdh->dx_ = dest->x_;
    sdh->dy_ = dest->y_;
    sdh->crt_time_ = SPEED_CURRENT;
    sdh->insert_time_ = SPEED_CURRENT;
    sdh->hops_ = 1;
    sdh->seqno_ = pub_seqno_;
    sdh->e2e_delay_ = e2e_delay_;
    sdh->e2e_reliabitily_ = 0.5;

    double mydis = G::distance(this, dest);
    sdh->setpoint_ = mydis / sdh->e2e_delay_;
    nblist_->setSpeedSetPoint(sdh->setpoint_);

    iph->saddr() = my_id_;
    iph->daddr() = -1;
    iph->sport() = RT_PORT;
    iph->dport() = RT_PORT;
    iph->ttl_ = 6 * IP_DEF_TTL;    // max hop-count

    F2list *f1 = nblist_->nexthop(speed_type_,dest->x_,dest->y_,sdh->e2e_reliabitily_);

    if (f1->node_list_->id_ == -1) {
        dumpDrop("NRTE", sdh);
        drop(p, "NoRt");
        backpressure(0, dest->x_, dest->y_);
        return;
    }
    if (f1->node_list_->id_ == -2) {
        dumpDrop("CONG", sdh);
        drop(p, "Cong");
        backpressure(1, dest->x_, dest->y_);
        return;
    }

    node *F2 = f1->node_list_;
    do{
        Packet *Pi = allocpkt();
        struct hdr_cmn *cmhPi = HDR_CMN(Pi);
        struct hdr_ip *iphPi = HDR_IP(Pi);
        struct hdr_speed_data *sdhPi = HDR_SPEED_DATA(Pi);
        cmhPi->direction() = hdr_cmn::DOWN;
        cmhPi->addr_type_ = NS_AF_INET;
        cmhPi->size() = sdh->size() + IP_HDR_LEN;

        iphPi->daddr() = IP_BROADCAST << Address::instance().nodeshift();
        iphPi->sport() = RT_PORT;
        iphPi->dport() = RT_PORT;
        iphPi->ttl_ = 1;

        sdhPi->type_ = SPEEDTYPE_DATA;
        sdhPi->src_ = my_id_;
        sdhPi->dst_ = iph->daddr();;
        sdhPi->last_hop_ = my_id_;
        sdhPi->dx_ = dest->x_;
        sdhPi->dy_ = dest->y_;
        sdhPi->crt_time_ = SPEED_CURRENT;
        sdhPi->insert_time_ = SPEED_CURRENT;
        sdhPi->hops_ = 1;
        sdhPi->seqno_ = pub_seqno_;
        sdhPi->e2e_delay_ = e2e_delay_;

        sdhPi->e2e_reliabitily_ = F2->x_;
        cmhPi->next_hop_ = F2->id_;
        F2 = F2->next_;
        send(Pi, 0);

    } while (F2);

}

void
SpeedAgent::backpressure(int type, double sx, double sy) {
    if (speed_type_ != 0) return;
    bkp_counter_++;

    Packet *p = allocpkt();
    struct hdr_cmn *cmh = HDR_CMN(p);
    struct hdr_ip *iph = HDR_IP(p);
    struct hdr_speed_bkp *sbh = HDR_SPEED_BKP(p);

    cmh->next_hop_ = IP_BROADCAST;
    cmh->addr_type_ = NS_AF_INET;
    cmh->ptype() = PT_SPEED;
    cmh->size() = sbh->size() + IP_HDR_LEN;

    iph->daddr() = IP_BROADCAST << Address::instance().nodeshift();
    iph->sport() = RT_PORT;
    iph->dport() = RT_PORT;
    iph->ttl_ = 1;

    sbh->type_ = SPEEDTYPE_BKP;
    sbh->id_ = my_id_;
    sbh->sx_ = sx;
    sbh->sy_ = sy;

    if (type == 0)
        sbh->delay_ = INFINITE_DELAY;
    else {
        sbh->delay_ = nblist_->avg2delay(sx, sy);
        //    nblist_->dump(); //for back pressure debug
    }

#ifdef BKP_DUMP
    FILE *fp = fopen("backpressure.tr", "a+");
  fprintf(fp, "%.8f\t%d\t%d\t%.2f\t%.2f\t%.8f\n",
          SPEED_CURRENT, type, my_id_, my_x_, my_y_, sbh->delay_);
  fclose(fp);
  //  nblist_->dump(sx, sy);
#endif

    send(p, 0);


}

int
SpeedAgent::command(int argc, const char *const *argv) {
    if (argc == 2) {
        if (strcasecmp(argv[1], "start") == 0) {
            startUp();
            return TCL_OK;
        }
        if (strcasecmp(argv[1], "dumpEnergy") == 0) {
            return TCL_OK;
        }
        if (strcasecmp(argv[1], "dumpBroadcast") == 0) {
            return TCL_OK;
        }
        if (strcasecmp(argv[1], "dump") == 0) {
            dumpNeighbor();
            dumpEnergy();
            return TCL_OK;
        }
    }

    if (argc == 3) {
        if (strcmp(argv[1], "addr") == 0) {
            my_id_ = Address::instance().str2addr(argv[2]);
            return TCL_OK;
        }  else if (strcasecmp(argv[1], "type") == 0) {
        e2e_delay_ = atof(argv[2]);
        return (TCL_OK);
    }

    TclObject *obj;
        if ((obj = TclObject::lookup(argv[2])) == 0) {
            fprintf(stderr, "%s: %s lookup of %s failed\n", __FILE__, argv[1],
                    argv[2]);
            return (TCL_ERROR);
        }
        if (strcasecmp(argv[1], "node") == 0) {
            node_ = (MobileNode *) obj;
            return (TCL_OK);
        } else if (strcasecmp(argv[1], "port-dmux") == 0) {
            port_dmux_ = (PortClassifier *) obj;
            return (TCL_OK);
        } else if (strcasecmp(argv[1], "tracetarget") == 0) {
            trace_target_ = (Trace *) obj;
            return TCL_OK;
        }
//        if (strcmp(argv[1], "access-mac") == 0) {
//            mac_pt_ = (Mac802_11*)TclObject::lookup(argv[2]);
//            if (mac_pt_ == 0) {
//                fprintf (stderr, "Agent: %s lookup %s failed. \n", argv[1], argv[2]);
//                return TCL_ERROR;
//            }
//            return TCL_OK;
//        }
    }// if argc == 3

    return (Agent::command(argc, argv));
}

void
SpeedAgent::newdelay(nsaddr_t nid, double isrt_time, double sx, double sy) {
    double delay = SPEED_CURRENT - isrt_time;
    checkmiss(nid, delay, sx, sy);
    nblist_->updateDelay(nid, delay, sx, sy);

}

void
SpeedAgent::collide(nsaddr_t nid) {
    nblist_->collide(nid);
    miss_counter_++;
}

void
SpeedAgent::recv(Packet *p, Handler *h) {
    struct hdr_cmn *cmh = HDR_CMN(p);
    struct hdr_ip *iph = HDR_IP(p);
    struct hdr_speed *sh = HDR_SPEED(p);

    switch (cmh->ptype()) {
        case PT_CBR:
            if (iph->saddr() == my_id_)                // a packet generated by myself
            {
                if (cmh->num_forwards() == 0)        // a new packet
                {
                    pubdata(p);
                } else    //(cmh->num_forwards() > 0)	// routing loop -> drop
                {
                    drop(p, DROP_RTR_ROUTE_LOOP);
                }
            } else {
                iph->ttl_--;
                if (iph->ttl_ == 0) {
                    drop(p, DROP_RTR_TTL);
                    return;
                }
                recvData(p);
            }
            break;
        case PT_SPEED:
            switch (sh->type_) {
                case SPEEDTYPE_HELLO:
                    recvHello(p);
                    break;
                case SPEEDTYPE_DATA:
                    recvData(p);
                    break;
                case SPEEDTYPE_ACK:
                    recvAck(p);
                    break;
                case SPEEDTYPE_BKP:
                    recvBkp(p);
                    break;

                default:
                    printf("Wrong packet type.\n");
                    exit(1);
            }
            break;

        default:
            drop(p, " UnknowType");
            break;
    }
}

void
SpeedAgent::recvHello(Packet *p) {
    struct hdr_speed_hello *shh = HDR_SPEED_HELLO(p);

    nblist_->newNB(shh->id_, shh->x_, shh->y_,
                   SPEED_CURRENT - shh->insert_time_, shh->seqno_);

    drop(p, "HELLO");
}

void
SpeedAgent::recvData(Packet *p) {

    struct hdr_cmn *cmh = HDR_CMN(p);

    if (cmh->next_hop_ != my_id_) {
        drop(p, "ErNH");//error next hop
        return;
    }

    struct hdr_speed_data *sdh = HDR_SPEED_DATA(p);

    sendAck(sdh->last_hop_, sdh->insert_time_, sdh->dx_, sdh->dy_);

    if (cmh->direction() == hdr_cmn::UP && sdh->dst_ == my_id_) {
        dumpDrop("RECEIVED", sdh);
        drop(p, "DATA_RECEIVED");
    } else {
        relayData(p);
    }
}

void
SpeedAgent::recvBkp(Packet *p) {
    struct hdr_speed_bkp *sbh = HDR_SPEED_BKP(p);

    nblist_->updateDelay(sbh->id_, sbh->delay_, sbh->sx_, sbh->sy_);

    drop(p, "BKP_RECEIVED");
}

void
SpeedAgent::relayData(Packet *p) {
    struct hdr_speed_data *sdh = HDR_SPEED_DATA(p);
    struct hdr_ip *iph = HDR_IP(p);
    double ssx = sdh->dx_;
    double ssy = sdh->dy_;

    double elapsed = SPEED_CURRENT - sdh->crt_time_;
    if (sdh->e2e_delay_ - elapsed <= 0) {
        dumpDrop("TIMEOUT", sdh);
        drop(p, "TimeOut");
        backpressure(1, ssx, ssy);
        return;
    }

    nblist_->setSpeedSetPoint(sdh->setpoint_);

    F2list *f1 = nblist_->nexthop(speed_type_,dest->x_,dest->y_,sdh->e2e_reliabitily_);

    if(f1 == NULL) {
        double mydis = G::distance(this->x_, this->y_, ssx, ssy);
        double elapsed_time = SPEED_CURRENT - sdh->crt_time_;
        sdh->setpoint_ = mydis / (sdh->e2e_delay_ - elapsed_time);
        nblist_->setSpeedSetPoint(sdh->setpoint_);
        f1 = nblist_->nexthop(speed_type_,ssx,ssy,sdh->e2e_reliabitily_);
    }

    if (f1->node_list_->id_ == -1) {
        dumpDrop("NRTE", sdh);
        drop(p, "NoRt");
        backpressure(0, ssx, ssy);
        return;
    }
    if (f1->node_list_->id_ == -2) {
        dumpDrop("CONG", sdh);
        drop(p, "Cong");
        backpressure(1, ssx, ssy);
        return;
    }


    node *F2 = f1->node_list_;
    do{
        Packet *Pi = allocpkt();
        struct hdr_cmn *cmhPi = HDR_CMN(Pi);
        struct hdr_ip *iphPi = HDR_IP(Pi);
        struct hdr_speed_data *sdhPi = HDR_SPEED_DATA(Pi);
        cmhPi->direction() = hdr_cmn::DOWN;
        cmhPi->addr_type_ = NS_AF_INET;
        cmhPi->size() = sdh->size() + IP_HDR_LEN;

        iphPi->daddr() = IP_BROADCAST << Address::instance().nodeshift();
        iphPi->sport() = RT_PORT;
        iphPi->dport() = RT_PORT;
        iphPi->ttl_ = 1;

        sdhPi->type_ = SPEEDTYPE_DATA;
        sdhPi->src_ = my_id_;
        sdhPi->dst_ = iph->daddr();;
        sdhPi->last_hop_ = my_id_;
        sdhPi->dx_ = dest->x_;
        sdhPi->dy_ = dest->y_;
        sdhPi->crt_time_ = SPEED_CURRENT;
        sdhPi->insert_time_ = SPEED_CURRENT;
        sdhPi->hops_ = 1;
        sdhPi->seqno_ = pub_seqno_;
        sdhPi->e2e_delay_ = e2e_delay_;

        sdhPi->e2e_reliabitily_ = F2->x_;

        cmhPi->direction() = hdr_cmn::DOWN;
        cmhPi->next_hop_ = F2->id_;
        iphPi->ttl_--;

        sdhPi->insert_time_ = SPEED_CURRENT;
        sdhPi->hops_++;
        F2 = F2->next_;

 //       fwd_counter_++;

        send(Pi, 0);
    } while (F2);

}

void
SpeedAgent::sendAck(nsaddr_t nid, double crtt, double sx, double sy) {
    Packet *p = allocpkt();

    struct hdr_cmn *cmh = HDR_CMN(p);
    struct hdr_ip *iph = HDR_IP(p);
    struct hdr_speed_ack *sah = HDR_SPEED_ACK(p);

    cmh->next_hop_ = nid;
    cmh->addr_type_ = NS_AF_INET;
    cmh->ptype() = PT_SPEED;
    cmh->size() = sah->size() + IP_HDR_LEN;

    iph->daddr() = IP_BROADCAST << Address::instance().nodeshift();
    iph->sport() = RT_PORT;
    iph->dport() = RT_PORT;
    iph->ttl_ = 1;

    sah->type_ = SPEEDTYPE_ACK;
    sah->id_ = my_id_;
    sah->dx_ = sx;
    sah->dy_ = sy;
    sah->delay_ = SPEED_CURRENT - crtt;

    send(p, 0);
}

void
SpeedAgent::recvAck(Packet *p) {
    struct hdr_cmn *cmh = HDR_CMN(p);
    struct hdr_speed_ack *sah = HDR_SPEED_ACK(p);

    if (cmh->next_hop_ != my_id_) return;

    double delay = sah->delay_;

    if (delay <= 0.0) {
        printf("Wrong delay.\n");
        return;
    }

    checkmiss(sah->id_, delay, sah->dx_, sah->dy_);

    nblist_->updateDelay(sah->id_, delay, sah->dx_, sah->dy_);

    drop(p, "ACK_RECEIVED");
}


void
SpeedAgent::checkmiss(nsaddr_t nid, double delay, double sx, double sy) {
    assert(delay > 0.0);

    double ndis = nblist_->getdis(nid, sx, sy);
    double tempx = this->x_ - sx;
    double tempy = this->y_ - sy;

    tempx = tempx * tempx;
    tempy = tempy * tempy;

    double mydis = sqrt(tempx + tempy);
    if (ndis > mydis) return;

    double tempspeed = (mydis - ndis) / delay;
    if (tempspeed < e2e_delay_) miss_counter_++;

}

void
SpeedAgent::startUp() {
    this->x_ = node_->X();        // get Location
    this->y_ = node_->Y();
    dest->x_ = node_->destX();
    dest->y_ = node_->destY();

    nblist_->myinfo(my_id_, this->x_, this->y_);

    nblist_->setpara(delay_alpha_, prob_k_, rrpg_k_);
    hello_timer_.resched(randHello_.uniform(0.0, 0.5));

    FILE *fp;
    fp = fopen("Neighbors.tr", "w");
    fclose(fp);
    fp = fopen("Drop.tr", "w");
    fclose(fp);
    if (node_->energy_model()) {
        fp = fopen("Energy.tr", "w");
        fclose(fp);
    }
}

void
SpeedAgent::dumpEnergy() {
    if (node_->energy_model()) {
        FILE *fp = fopen("Energy.tr", "a+");
        fprintf(fp, "%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", my_id_, this->x_, this->y_,
                node_->energy_model()->energy(),
                node_->energy_model()->off_time(),
                node_->energy_model()->et(),
                node_->energy_model()->er(),
                node_->energy_model()->ei(),
                node_->energy_model()->es()
        );
        fclose(fp);
    }
}

void
SpeedAgent::dumpNeighbor() {
    FILE *fp = fopen("Neighbors.tr", "a+");
    fprintf(fp, "%d	%f	%f	%f	", this->my_id_, this->x_, this->y_, node_->energy_model()->off_time());
    nblist_->dump(fp);
    fprintf(fp, "\n");
    fclose(fp);
}

void SpeedAgent::dumpDrop(const char *result, struct hdr_speed_data *sdh) {
    FILE *fp = fopen("Drop.tr", "a+");
    fprintf(fp, "%s\t%.8f\t%.8f\t%d\t%d\t%d\n",
            result, SPEED_CURRENT, sdh->crt_time_, sdh->src_, sdh->seqno_, sdh->hops_);
    fclose(fp);
}